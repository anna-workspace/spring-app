FROM openjdk:11.0
ADD target/userdatabase-0.0.1-SNAPSHOT.jar ./app.jar
ENV MONGODB_URI=mongodb://mongodb:27017/test
EXPOSE 8080
ENTRYPOINT ["java","-jar","./app.jar"]
